FROM debian:stable-slim

RUN apt update -y && apt dist-upgrade -y

RUN apt install -y slapd ldap-utils

RUN mkdir /var/log/slapd

VOLUME /var/lib/ldap
VOLUME /etc/ldap/slapd.d

EXPOSE 636
EXPOSE 389

ADD entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/sbin/slapd","-d","0","-h","ldap:///","-F","/etc/ldap/slapd.d"]
