#!/bin/sh

set -e

if [ -f /opt/config.ldif ]; then
	echo "begin: Update OpenLDAP config"
	slapcat -F /etc/ldap/slapd.d -b cn=config > /opt/`date +%Y%m%d`_config.ldif.backup
	touch /opt/.remove && rm -rf /etc/ldap/slapd.d/*
	slapadd -F /etc/ldap/slapd.d -b cn=config -l /opt/config.ldif
	mv /opt/config.ldif /opt/config.set.ldif
	echo "end: Update OpenLDAP config"
fi

if [ -f /opt/commonscloud.coop.ldif ]; then 
	echo "begin: Update OpenLDAP Commons Cloud data"
	slapcat -F /etc/ldap/slapd.d -b cn=config > /opt/`date +%Y%m%d`_config.ldif.backup
	touch /opt/.remove && rm -rf /var/lib/ldap/*
	slapadd -F /etc/ldap/slapd.d -b dc=commonscloud,dc=coop -l /opt/commonscloud.coop.ldif
	mv /opt/commonscloud.coop.ldif /opt/commonscloud.coop.set.ldif
	echo "end: Update OpenLDAP Commons Cloud data"
fi

exec $@
